#! /bin/bash

# This is a script which, when run on Mac or Linux, will automatically build the site, the index, commit and push it to SoureHut.

# The following creates variables which set the colour/formatting of the output to the terminal.
# Examples: Green is green, under is underlined, bu is bold and underlined, reset resets everything to normal.
green=$(tput setaf 2)
red=$(tput setaf 1)
yellow=$(tput setaf 3)
cyan=$(tput setaf 6)
bold=$(tput bold)
under=$(tput smul)
bu=$(tput bold && tput smul)
reset=$(tput sgr0)

echo -e "${bold}Deploying updates to SourceHut...${reset}"

# Build index for site using the generate.sh script (see file for more info).
# Add changes to git for all directories.
echo -e "${cyan}Tracking all git files...${reset}"
git add .

# Commit changes.
echo -e "${cyan}What is the custom commit message?${reset}"
read -r usrmsg

echo -e "${cyan}Committing changes...${reset}"
msg="rebuilding site $(date)."
if [ $# -eq 1 ]
  then msg="$1"
fi
git commit -m "$usrmsg: $msg"
echo -e "${green}Changes committed.${reset}"

# Push source and build repos.
echo -e "${cyan}Pushing origin master...${reset}"
git push origin master
echo -e "${green}Pushed.${reset}"

echo -e "${green}LOCAL PROCESS COMPLETED.${reset}"
