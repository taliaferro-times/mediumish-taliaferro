---
title: "{{ replace .Name "-" " " | title }}"
author:
date: {{ .Date }}
publishDate: {{ .Date }}
description:
resources:
  - name: preview
    src: 
imageDescription: ""
imageAttribution: ""
tags: [""]
comments: false
draft: true
tableOfContents: false
pandoc: false
updated: false
---
