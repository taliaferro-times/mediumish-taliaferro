## The Taliaferro Mediumish Theme

A theme for The Taliaferro Times, derived from the Mediumish Hugo theme by lgaida on GitHub.

### Added Features
- Search via Lunr.js
- Responsive image creation
- Uses yaml instead of toml
- Works as a multi-user news platform instead of a single-author blog (main page is blog)
- Better share options (Facebook, Twitter, Yandex, and custom Mastodon script)
- Adds GoatTracker analytics
- Each post on a post page shows author name with link to all of author's posts
- Dark mode via DarkReader scripts  
- Git information (date & hash) on each post

### Under Development
- Easy integration of Pandoc to create PDF, DOCX, TXT, RTF, ODT, and other versions of each page
